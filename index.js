'use strict';

var redis = require('redis').createClient(process.env.REDIS_URL);
const Hapi = require('hapi');

const server = new Hapi.Server();

server.connection({port: process.env.PORT || 9988});

server.route({
  method: 'GET',
  path: '/{id}',
  handler: (request, reply) =>  {
    redis.get(request.params.id, (err, data) => {
        if (err) 
            reply({status: 'Unknown'});

        if (data)
            reply({status: data.toString()});
    });
  }
});

server.route({
  method: 'POST',
  path: '/',
  handler: (request, reply) =>  {
      redis.set(request.payload.id, request.payload.status, redis.print);
      reply().created('/' + request.payload.id);
  }
});

server.start((err) => {
  if (err) throw err;
  console.log('Server running at: ' + server.info.uri);
});

redis.on('error', (err) => {
    console.log('Redis error: ' + err);
});